import { Injectable, Optional } from '@angular/core';
import { LoggerLevel } from './logger.levels';

export class LoggerServiceConfig {
  level: LoggerLevel = LoggerLevel.WARN;
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  static LogLevelFromString(level: string): LoggerLevel {
    if (!level) {
      return undefined;
    }

    const l = level.toLowerCase();

    switch (l) {
      case 'trace':
        return LoggerLevel.TRACE;
      case 'debug':
        return LoggerLevel.DEBUG;
      case 'info':
        return LoggerLevel.INFO;
      case 'log':
        return LoggerLevel.LOG;
      case 'warn':
        return LoggerLevel.WARN;
      case 'error':
        return LoggerLevel.ERROR;
      case 'fatal':
        return LoggerLevel.FATAL;
      case 'off':
        return LoggerLevel.OFF;
    }
    return undefined;
  }

  constructor(@Optional() config?: LoggerServiceConfig) {
    let level = LoggerLevel.WARN;

    if (config) {
      level = config.level;
    }
    this.setLogLevel(level);
  }

  public setLogLevelFromString(level: string): void {
    const newLevel = LoggerService.LogLevelFromString(level);
    if (newLevel !== undefined) {
      this.setLogLevel(newLevel);
    }
  }

  private NO_LOG = () => { };

  public setLogLevel(level: LoggerLevel): void {

    if (level <= LoggerLevel.LOG) {
      // tslint:disable-next-line
      this.log = console.log.bind(console);
    } else {
      this.log = this.NO_LOG;
    }

    if (level <= LoggerLevel.DEBUG) {
      // tslint:disable-next-line
      this.debug = console.debug.bind(console);
    } else {
      this.debug = this.NO_LOG;
    }

    if (level <= LoggerLevel.WARN) {
      // tslint:disable-next-line
      this.warn = console.warn.bind(console);
    } else {
      this.warn = this.NO_LOG;
    }

    if (level <= LoggerLevel.ERROR) {
      // tslint:disable-next-line
      this.error = console.error.bind(console);
    } else {
      this.error = this.NO_LOG;
    }
  }

  public log(message?: any, ...optionalParams: any[]): void { }
  public debug(message?: any, ...optionalParams: any[]): void { }
  public warn(message?: any, ...optionalParams: any[]): void { }
  public error(message?: any, ...optionalParams: any[]): void { }
}

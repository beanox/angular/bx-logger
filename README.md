# Bx Logger

Tiny package for easy message logging in chrome

## Install

```bash
npm i @beanox/bx-logger
```

## Import and configure app.config.ts

```typescript
import { BxLoggerModule, LoggerLevel } from '@beanox/bx-logger';

export const appConfig: ApplicationConfig = {
  providers: [
    ....
    importProvidersFrom(BxLoggerModule.forRoot({ level: LoggerLevel.TRACE })),
  ],
};
```

## Usage

```typescript

export class SomeComponentOrService {
      logger = inject(LoggerService);

    constructor() {}

     private someMethod() {
        this.logger.debug('this is debug message');
        this.logger.trace('this is trace message');

        ...

        // change log level
        this.logger.setLogLevel(LoggerLevel.WARN);
        // or
        this.logger.setLogLevelFromString('warn');
     }

```
